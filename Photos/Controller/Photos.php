<?php

namespace Photos\Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class Photos {
    
    /**
     * Endpoint for /photos.  Returns information about all users of the system.
     * 
     * @param \Silex\Application $app
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function actionIndex(Application $app) {
        $lib = $app['photos.library'];
        $users = $lib->getAllUsers();
        if ($app['negotiator.preferred'] == 'html') {
            $output = $app['twig']->render('userlist.twig', array('users' => $users));
            return new Response($output);
        }
        else if ($app['negotiator.preferred'] == 'json') {
            return new JsonResponse(array('users' => $users));
        }
        
        return new Response("Hello, Silex World!");
    }
    
    /**
     * Endpoint for /photos/<fbUserId>.  Returns information about a user.
     * 
     * @param \Silex\Application $app
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function actionUserInfo(Application $app, $id) {
        $lib = $app['photos.library'];
        $profile = $lib->getUserProfile($id);
        if ($app['negotiator.preferred'] == 'html') {
            $output = $app['twig']->render('profile.twig', array('user' => $profile));
            return new Response($output);
        }
        else if ($app['negotiator.preferred'] == 'json') {
            return new JsonResponse($profile);
        }
    }
    
    /**
     * Endpoint for for /photos/<fbUserId>.  Returns the user's photos using signed, expiring URLs
     * 
     * @param \Silex\Application $app
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function actionUserPhotos(Application $app, $id) {
        $lib = $app['photos.library'];
        $photos = $lib->getAllPhotosForUser($id);
        $user = $lib->getUserProfile($id);
        if ($app['negotiator.preferred'] == 'html') {
            $output = $app['twig']->render('userphotos.twig', array(
                'user' => $user,
                'photos' => $photos
            ));
            return new Response($output);
        }
        else if ($app['negotiator.preferred'] == 'json') {
            return new JsonResponse(array('photoList' => $photos, 'user' => $user));
        }
    }
}
