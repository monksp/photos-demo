<?php

namespace Photos;

class Library {
    /*
     * @var signedUrlDuration The length of time signed URLs live
     */
    protected $signedUrlDuration = '+10 minutes';
    
    /*
     * @var client An AWS s3 client
     */
    protected $client = null;
    
    /*
     * @var bucketName The name of the S3 bucket where photos live
     */
    protected $bucketName = null;
    
    /*
     * @var facebook The facebook API instance
     */
    protected $facebook = null;
    
    /**
     * Creates a new instance of the photo library
     *
     * @param   AWS\S3Client  $s3client        A connector to S3
     * @param   string        $bucketName      The name of the S3 bucket for photos
     * @param   Facebook      $facebookClient  The Facebook API connector
     * @return  void
     */
    public function __construct($s3client, $bucketName, $facebookClient) {
        $this->client = $s3client;
        $this->bucketName = $bucketName;
        $this->facebook = $facebookClient;
    }

    /**
     * Creates a list of the name, facebook URL, and local application profile and photo
     * pages.
     *
     * @return  array
     */
    public function getAllUsers() {
        $client = $this->client;
        $iterator = $client->listObjects(array(
            'Bucket' => $this->bucketName,
            'Delimiter' => '/'
        ));
        
        $users = array();
        foreach ($iterator->get('CommonPrefixes') as $prefix) {
            preg_match('@facebook-(?P<fbId>\d+)/@', $prefix['Prefix'], $matches);
            if (array_key_exists('fbId', $matches)) {
                $users[] = $matches['fbId'];
            }
        }

        $collected_user_data = array();
        foreach ($users as $user) {
            $fbData = $this->facebook->api('/' . $user . '?fields=name,link', 'GET');
            $collected_user_data[] = array(
                'name' => $fbData['name'],
                'facebookUrl' => $fbData['link'],
                'profileUrl' => '/photos/' . $user,
                'photoUrl' => '/photos/' . $user . '/photos'
            );
        }
        
        return $collected_user_data;
    }
    /**
     * Returns profile data for a given FB id
     *
     * @param   int  $id  A Facebook User ID
     * @return  array
     */
    public function getUserProfile($id) {
        $profile = $this->facebook->api('/' . $id, 'GET');
        $importantBits = array(
            'name' => $profile['name'],
            'facebookId' => $profile['id'],
            'facebookLink' => $profile['link'],
            'profilePicture' => 'http://graph.facebook.com/' . $profile['id'] . '/picture?type=large',
            'photosLink' => '/photos/' . $profile['id'] . '/photos',
            'photoCount' => $this->getPhotoCountForUser($profile['id'])
        );
        
        return $importantBits;
    }

    /**
     * Returns an array containing both S3 keys and presigned URLs for all photos
     * in the system
     * 
     * @param  int  $id  Optional.  A Facebook User ID to filter the photos by
     * 
     * @return array
     */
    public function getAllPhotos($id = null) {
        $client = $this->client;
        $params = array(
            'Bucket' => $this->bucketName
        );
        if ($id !== null) {
            $params['Prefix'] = 'facebook-' . $id;
        }
        $iterator = $client->getIterator('ListObjects', $params);
        
        $photos = array();
        $end_time = date('Y-m-d H:i:s', strtotime($this->signedUrlDuration));
        foreach ($iterator as $object) {
            $photos[] = array(
                'key' => $object['Key'],
                'url' => $this->retrieveSignedUrl($object['Key'])
            );
        }
        
        return array('urlExpirationTime' => $end_time, 'photos' => $photos);
    }
    /**
     * Just a wrapper around getAllPhotos to be a little more Clean.
     *
     * @param  int  $id  A Facebook User ID to filter the photos by
     * @return  array
     */
    public function getAllPhotosForUser($userId) {
        return $this->getAllPhotos($userId);
    }
    /**
     * Counts the number of photos a given user has uploaded
     *
     * @param  int  $id  A Facebook User ID to filter the photos by
     * @return  int
     */
    public function getPhotoCountForUser($userId) {
        $photos = $this->getAllPhotos($userId);
        return count($photos['photos']);
    }
    
    /**
     * Creates a signed URL from an object key
     *
     * @param   string  $key  An object key
     * @return  string
     */
    public function retrieveSignedUrl($key) {
        $fullUrl = $this->bucketName . '/' . $key;
        $request = $this->client->get($fullUrl);
        
        $signedUrl = $this->client->createPresignedUrl($request, $this->signedUrlDuration);
        
        return $signedUrl;
    }
}
