<?php

namespace Photos\Silex\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

class FacebookServiceProvider implements ServiceProviderInterface {

    public function register(Application $app) {
        $app['facebook'] = $app->share(function() use ($app) {
            return new \Facebook(array(
                'appId' => $app['facebook.appId'],
                'secret' => $app['facebook.appSecret'],
            ));
        });
    }

    public function boot(Application $app) {}

}