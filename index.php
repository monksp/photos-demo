<?php
error_reporting(E_ALL | E_STRICT);

require_once __DIR__.'/vendor/autoload.php';
$app = new Silex\Application();

// Uncomment to turn on debugging
//$app['debug'] = true;

// Setup AWS
$app->register(new Aws\Silex\AwsServiceProvider(), array(
    'aws.config' => array(
        'key'    => 'AWS_ACCESS_KEY',
        'secret' => 'AWS_SECRET_KEY',
        'region' => 'us-east-1', // Or wherever
    ))
);
// Setup FB
$app['facebook.appId'] = 'FACEBOOK_APP_ID';
$app['facebook.appSecret'] = 'FACEBOOK_APP_SECRET';
$app->register(new Photos\Silex\Provider\FacebookServiceProvider());

// Setup Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/Photos/views',
    'twig.options' => array('cache' => __DIR__ . '/Photos/views/cache')
));

// Setup content negotiator
$app['negotiator'] = $app->share(function($app) {
    return new \Negotiation\FormatNegotiator();
});
$app['negotiator.preferred'] =  $app->share(function($app) {
    $acceptHeader = $_SERVER['HTTP_ACCEPT'];
    $priorities = array('json', 'html', '*/*');
    return $app['negotiator']->getBestFormat($acceptHeader, $priorities);
});

// Setup Photo Library
$app['photos.bucket'] = 'monksp-photo-library';
$app['photos.library'] = $app->share(function($app) {
    return new Photos\Library($app['aws']->get('s3'), $app['photos.bucket'], $app['facebook']);
});

// Routing
$app->get('/photos', 'Photos\Controller\Photos::actionIndex');
$app->get('/photos/{id}', 'Photos\Controller\Photos::actionUserInfo')
        ->assert('id', '\d+');
$app->get('/photos/{id}/photos', 'Photos\Controller\Photos::actionUserPhotos')
        ->assert('id', '\d+');

// Hey ho, here we go!
$app->run();

