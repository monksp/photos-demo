(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

(function($) {
    var Application = function(options) { this._init(options); }

    Application.prototype = {
        roleArn: null,
        bucketName: null,
        bucket: null,

        controls: null,
        results: null,
        thumbnails: null,
        uploadButton: null,
        _init: function(options) {
            this.roleArn = options.roleArn;
            this.bucketName = options.bucketName;
            this.bucket = new AWS.S3({params: {Bucket: options.bucketName}});

            // Grab some DOM elements that we'll use repeatedly.
            this.controls = $("#controls");
            this.results = $("#results");
            this.thumbnails = $("#thumbnails");
            this.uploadButton = $("#upload-button");
        },
        // Toggle between the upload screen and the results panel
        showControls: function() {
            this.controls.removeClass("hide");
            this.results.addClass("hide").empty();
        },
        showResults: function() {
            this.controls.addClass("hide");
            this.results.removeClass("hide");
        },
        // Simple appender
        addToResults: function(stuff) {
            $(stuff).appendTo(this.results);
        },
        // Not really a folder, just a prefix for the uploaded photo's key
        userFolder: function(fbUserId) {
            // Default to current user's photos
            if (!fbUserId) {
                fbUserId = this.loginData.id;
            }
            return 'facebook-' + fbUserId.toString();
        },
        // Retrieve data about all users that have already uploaded photos
        loadAllUsers: function() {
            this.bucket.listObjects({Delimiter: "/"}, function(error, data) {
                for (var i in data.CommonPrefixes) {
                    var id = data.CommonPrefixes[i].Prefix.replace(/facebook-/, "").replace(/\//, "");
                    FB.api("/" + id, function(user) {
                        $("#individual-photos").mustache("user-line", {
                            name: user.name,
                            id: id
                        });
                    });
                }
            });
        },
        // Load all photos for a given user
        loadUserPhotos: function(userId) {
            var photos = [];
            
            var that = this;
            this.bucket.listObjects({Prefix: this.userFolder(userId)}, function(error, data) {
                // Everything's uploaded as private, so we need signed URLs for each photo
                for (var i in data.Contents) {
                    var object = data.Contents[i],
                        params = {Key: object.Key},
                        url = that.bucket.getSignedUrl("getObject", params),
                        date = object.LastModified;
                    photos.push({
                        url: url,
                        date: date
                    });
                }
                // Order by date descending
                photos.sort(function(a,b){
                    a = new Date(a.date);
                    b = new Date(b.date);
                    return a < b ? 1 : a > b ? -1 : 0;
                });
                that.results.mustache("photo-list", {photos: photos}, {method: "html"});
                that.showResults();
            });
        },
        
        // Compiled login data for the current user, and reveals the full interface
        completeFBLogin: function(response) {
            var fbUserId = response.authResponse.userID;

            // Config the bucket connection with temporary credentials
            this.bucket.config.credentials = new AWS.WebIdentityCredentials({
                ProviderId: 'graph.facebook.com',
                RoleArn: this.roleArn,
                WebIdentityToken: response.authResponse.accessToken
            });
            
            var that = this,
                loginDataBox = $("#facebook-logged-in");
            FB.api("/" + fbUserId, function(results) {
                that.loginData = results;
                that.loginData.photo = "http://graph.facebook.com/" + fbUserId + "/picture";

                // Update the DOM
                loginDataBox
                    .find("#current-facebook-user").html(that.loginData.name).end()
                    .find("img").attr("src", that.loginData.photo).end()
                    .find("a").attr("href", that.loginData.link).end()
                    .removeClass("hide");
                $("a.my").data("id", fbUserId);
                $("#facebook-login-needed").addClass("hide");
                $(".logged-in-only").addClass("logged-in");
            });

            // Kick off the process of grabbing the user list
            that.loadAllUsers();
        }
    };

    $(function() {
        var appId = 'FACEBOOK_APP_ID';
        var roleArn = 'AWS_ARN';
        var bucketName = 'AWS_S3_BUCKET_NAME';

        var app = new Application({
            roleArn: roleArn,
            bucketName: bucketName
        });


        window.fbAsyncInit = function() {
            FB.Event.subscribe('auth.statusChange', function(response) {
                if (response.status === 'connected') {
                    app.completeFBLogin(response);
                }
                else {
                    $("#facebook-login-needed").removeClass("hide");
                    $("#facebook-login-button").click(function() {
                        FB.login(app.completeFBLogin);
                    });
                }
            });
            FB.init({
                appId: appId,
                status: true
            });
        };
        
        // Load Templates
        $("#templates .mustache-template").each(function(idx, element) {
            var $element = $(element),
                template = $element.html(),
                templateName = element.id.replace(/mustache-/, "");
            $.Mustache.add(templateName, template); 
        });
        
        // DOM Bindings
        $("a.upload").on("click", function(ev) {
            app.showControls();
            
            ev.preventDefault();
            ev.stopPropagation();
            return false;
        });
        $("a.my").on("click", function(ev) {
            app.loadUserPhotos($(this).data("id"));
            
            ev.preventDefault();
            ev.stopPropagation();
            return false;
        });
        $("#individual-photos").on("click", "a", function(ev) {
            var id = $(this).data("user-id");
            app.loadUserPhotos(id);
            
            $("#individual-photos").dropdown("toggle");
            ev.preventDefault();
            ev.stopPropagation();
            return false;
        });
        
        app.thumbnails.on("click", ".remove", function(ev) {
            $(this).closest(".thumbnail").remove();
            if (app.thumbnails.find(".thumbnail").size() === 0) {
                app.uploadButton.addClass("hide");
            }
        });
        // Use the File API to thumbnail photos before upload
        $("#file-uploads").on("change", function() {
            var files = this.files;

            app.thumbnails.empty();
            
            for (var i = 0; i < files.length; i++) {
                var f = files[i];
                if (!f.type.match('image.*')) {
                  continue;
                }

                var reader = new FileReader();
                reader.onload = (function(file) {
                    return function(ev) {
                        var div = $("<div></div>", {"class": "thumbnail"});
                        $("<img />", {src: ev.target.result}).appendTo(div);
                        $("<i>").addClass("fa fa-trash-o remove").appendTo(div);
                        div.prependTo(app.thumbnails);
                        div.data("file", file);
                        
                        app.uploadButton.removeClass("hide");
                    };
                })(f);

                reader.readAsDataURL(f);
            }        
        });
        
        $("#upload-button").on("click", function(ev) {
            app.thumbnails.find(".thumbnail").each(function(idx, element) {
                var file = $(element).data("file"),
                    key = app.userFolder() + "/" + file.name,
                    params = {Key: key, ContentType: file.type, Body: file, ACL: "private"};
                    app.bucket.putObject(params, function(error, data) {
                        $(element).remove();
                    });
            });
            
            ev.preventDefault();
            ev.stopPropagation();
            return false;
        });

    });

})(jQuery);

