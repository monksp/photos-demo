Photo Sharing Demo Application
---

This is a quick example of using both the Amazon Web Services in-browser JavaScript SDK as well as the PHP SDK to create a simple photo sharing mechanism.

The JavaScript version is the most complete.  It features Facebook-based authentication through the AWS federated identity management system, and photo previewing using the File API.  There are no fallbacks and limited cross-browser compatibility, as this application is designed just for a small circle of local users.

The PHP components are built on top of Silex and is entirely read-only, but also features no access management.  It pulls data from both AWS and Facebook, and will output both HTML and JSON based on the Accept header's settings.

Setup
---
To use, you will need a Facebook application that allows Facebook logins on third party sites, and an Amazon Web Services account.

The JavaScript components require the Facebook App ID, the ARN for the AWS IAM role, and the name of the AWS S3 bucket.  It retrieves its credentials from the identity token vending machine inside the AWS infrastructure.

The PHP components require the Facebook App/Secret IDs and the AWS Access and Secret IDs and is not designed to work with EC2 IAM roles yet.

The AWS IAM role consists of two policy documents which are detailed here.

The first grants read/write access to the S3 bucket, in a folder described by the logged in Facebook ID

    {
      "Version": "2012-10-17",
      "Statement": [
    {
      "Action": [
        "s3:PutObject",
        "s3:PutObjectAcl",
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::S3_BUCKET_NAME/facebook-${graph.facebook.com:id}/*"
      ],
      "Effect": "Allow"
    }
  ]
}

The second grants read access to the entire S3 bucket, so users can browse other users' photos.

    {
      "Version": "2012-10-17",
      "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:Get*",
        "s3:List*"
      ],
      "Resource": "arn:aws:s3:::S3_BUCKET_NAME"
    }
  ]
}

